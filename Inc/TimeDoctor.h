/*
 * Copyright Thi Ngoc An Tran 2019.
 * All rights reserved.
 */

#ifndef TIMEDOCTOR_H
#define TIMEDOCTOR_H

#include <main.h>

/////////////////////// FreeRTOS defines /////////////////////////
#define configUSE_TRACE_FACILITY 				1

// not strictly needed, but may be handy for Wittenstein Stateviewer etc.
#define configGENERATE_RUN_TIME_STATS           1

#define configUSE_STATS_FORMATTING_FUNCTIONS	1

#define portCONFIGURE_TIMER_FOR_RUN_TIME_STATS() \
	do { \
		DWT->LAR = 0xC5ACCE55; \
		CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk; \
		DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk; \
	} while(0)

// DWT->CYCCNT = 0;

#define portGET_RUN_TIME_COUNTER_VALUE() (DWT->CYCCNT)

//////////////////////////////////////////////////////////////////

void TimeDoctor_Setup( UART_HandleTypeDef* huart, TIM_HandleTypeDef* htim, unsigned long timer_speed );

void TimeDoctor_traceSTART();
void TimeDoctor_traceEND();

void TimeDoctor_traceCREATE( uint32_t uxTaskNumber, char* taskName );
void TimeDoctor_traceTASK_SWITCHED_IN( uint32_t uxTaskNumber );
void TimeDoctor_traceTASK_SWITCHED_OUT( uint32_t uxTaskNumber );
void TimeDoctor_traceTASK_DELETE( uint32_t uxTaskNumber	);
void TimeDoctor_traceTASK_SUSPEND( uint32_t uxTaskNumber );
void TimeDoctor_traceTASK_RESUME( uint32_t uxTaskNumber );



void TimeDoctor_traceQUEUE_CREATE( void *pxNewQueue );
void TimeDoctor_traceQUEUE_REGISTRY_ADD( void *xQueue, uint32_t uxQueueNumber, const char* pcQueueName );
void TimeDoctor_traceQUEUE_SEND( uint32_t uxQueueNumber, uint32_t uxMessagesWaiting );
void TimeDoctor_traceQUEUE_RECEIVE( uint32_t uxQueueNumber, uint32_t uxMessagesWaiting );
void TimeDoctor_traceQUEUE_DELETE( uint32_t uxQueueNumber );
void TimeDoctor_traceQUEUE_SEND_FROM_ISR( uint32_t uxQueueNumber, uint32_t uxMessagesWaiting );
void TimeDoctor_traceQUEUE_RECEIVE_FROM_ISR( uint32_t uxQueueNumber, uint32_t uxMessagesWaiting );



void TimeDoctor_traceCREATE_COUNTING_SEMAPHORE();
void TimeDoctor_traceCREATE_MUTEX( void *pxNewMutex );
void TimeDoctor_traceTAKE_MUTEX_RECURSIVE( void *pxMutex, uint32_t uxQueueNumber );
void TimeDoctor_traceGIVE_MUTEX_RECURSIVE( void *pxMutex, uint32_t uxQueueNumber );


////////////// FreeRTOS trace macros ///////////////////


#define traceSTART() TimeDoctor_traceSTART();

#define traceEND() TimeDoctor_traceEND();



/* TASK-MACROS */
#define traceTASK_CREATE(pxNewTCB) TimeDoctor_traceCREATE( pxNewTCB->uxTCBNumber, pxNewTCB->pcTaskName );

#define traceTASK_SWITCHED_IN() TimeDoctor_traceTASK_SWITCHED_IN( pxCurrentTCB->uxTCBNumber );

#define traceTASK_SWITCHED_OUT() TimeDoctor_traceTASK_SWITCHED_OUT( pxCurrentTCB->uxTCBNumber );

#define traceTASK_DELETE( pxTaskToDelete ) TimeDoctor_traceTASK_DELETE( pxTaskToDelete->uxTCBNumber );

#define traceTASK_SUSPEND( pxTaskToSuspend ) TimeDoctor_traceTASK_SUSPEND( pxTaskToSuspend->uxTCBNumber);

#define traceTASK_RESUME( pxTaskToResume ) TimeDoctor_traceTASK_RESUME( pxTaskToResume->uxTCBNumber );

// these are not used right now
#define traceTASK_PRIORITY_INHERIT( pxTask, uxPriority )
#define traceTASK_PRIORITY_DISINHERIT( pxTask, uxPriority )
#define traceBLOCKING_ON_QUEUE_RECEIVE(pxQueue)
#define traceBLOCKING_ON_QUEUE_PEEK(pxQueue)
#define traceBLOCKING_ON_QUEUE_SEND(pxQueue)
#define traceMOVED_TASK_TO_READY_STATE(pxTCB)
#define tracePOST_MOVED_TASK_TO_READY_STATE(pxTCB)
#define traceTASK_DELAY_UNTIL(xTimeToWake)
#define traceTASK_DELAY()
#define traceTASK_PRIORITY_SET( pxTask, uxPriority )



/* QUEUE-MACROS */
#define traceQUEUE_CREATE(pxNewQueue) TimeDoctor_traceQUEUE_CREATE( pxNewQueue );

#define traceQUEUE_REGISTRY_ADD(xQueue, pcQueueName) TimeDoctor_traceQUEUE_REGISTRY_ADD( xQueue, ((Queue_t *) xQueue)->uxQueueNumber, pcQueueName );

#define traceQUEUE_SEND(pxQueue) TimeDoctor_traceQUEUE_SEND( pxQueue->uxQueueNumber, pxQueue->uxMessagesWaiting );

#define traceQUEUE_RECEIVE(pxQueue) TimeDoctor_traceQUEUE_RECEIVE( pxQueue->uxQueueNumber, pxQueue->uxMessagesWaiting );

#define traceQUEUE_DELETE( pxQueue ) TimeDoctor_traceQUEUE_DELETE( pxQueue->uxQueueNumber );

#define traceQUEUE_SEND_FROM_ISR( pxQueue )  TimeDoctor_traceQUEUE_SEND_FROM_ISR( pxQueue->uxQueueNumber, pxQueue->uxMessagesWaiting );

#define traceQUEUE_RECEIVE_FROM_ISR(pxQueue) TimeDoctor_traceQUEUE_RECEIVE_FROM_ISR( pxQueue->uxQueueNumber, pxQueue->uxMessagesWaiting );



/* SEMAPHORE-MACROS */
//#define traceCREATE_COUNTING_SEMAPHORE() TimeDoctor_traceCREATE_COUNTING_SEMAPHORE(); //ID 11



/* MUTEX-MACROS */

#define traceCREATE_MUTEX( pxNewQueue ) TimeDoctor_traceCREATE_MUTEX( pxNewQueue );

#define traceTAKE_MUTEX_RECURSIVE( pxMutex ) TimeDoctor_traceTAKE_MUTEX_RECURSIVE(pxMutex, ((Queue_t *) pxMutex)->uxQueueNumber );

#define traceGIVE_MUTEX_RECURSIVE( pxMutex ) TimeDoctor_traceGIVE_MUTEX_RECURSIVE(pxMutex, ((Queue_t *) pxMutex)->uxQueueNumber );

// StreamBuffer API

// Task Notification API

#endif
