/*
 * Copyright Thi Ngoc An Tran 2019.
 * All rights reserved.
 */

#include "TimeDoctor.h"

#include <stdio.h>
#include "main.h"

#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"

static char buffer[64000];
static int buffer_used = 0;

UART_HandleTypeDef *g_huart;	// for dumping the trace file
TIM_HandleTypeDef *g_htim;		// timer used for time stamping, should be as fast as possible
unsigned long g_timer_speed;


// Indicates the number of true clock cycles per second in the target system's time base.
// This tag must appear once as the first or second line in the file and is mandatory.
#define GET_CPU_CYCLES_PER_SECOND() HAL_RCC_GetHCLKFreq()

// Indicates the number of true clock memory cycles per second on the target system's memory bus.
// This tag must appear once as the third line in the file and is required only if "VAL 9" tags are used.
#define GET_MEM_CYCLES_PER_SECOND() HAL_RCC_GetHCLKFreq()

// Indicates the number of ticks per second that the STA/STO samples use as a time base.
// This value may be different from the value indicated by SPEED, but does not need to be.
// This tag must appear once as the first or second line in the file and is mandatory.
#define GET_TICKS_PER_SECOND()	g_timer_speed

// get current tick number for time stamping STA/STO
#define GET_TICK()	portGET_RUN_TIME_COUNTER_VALUE()



void TimeDoctor_Setup( UART_HandleTypeDef* huart, TIM_HandleTypeDef* htim, unsigned long timer_speed )
{
	g_huart = huart;
	g_htim = htim;
	g_timer_speed = timer_speed;
}

void TimeDoctor_traceSTART()
{
	if(buffer_used > sizeof(buffer)-64)
		return; // buffer full, do not write

	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();
	buffer_used += sprintf( buffer+buffer_used, "SPEED %lu\r\n", GET_CPU_CYCLES_PER_SECOND() );
	buffer_used += sprintf( buffer+buffer_used, "MEMSPEED %lu\r\n", GET_MEM_CYCLES_PER_SECOND() );
	buffer_used += sprintf( buffer+buffer_used, "TIME %lu\r\n", GET_CPU_CYCLES_PER_SECOND() );
	taskEXIT_CRITICAL_FROM_ISR(state);
}

void TimeDoctor_traceEND()
{
	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();
	buffer_used += sprintf( buffer+buffer_used, "END\r\n");
	HAL_UART_Transmit( g_huart, (uint8_t *)buffer, buffer_used, HAL_MAX_DELAY );
	taskEXIT_CRITICAL_FROM_ISR(state);
}

/* TRACE-MACROS FOR TASK */
void TimeDoctor_traceCREATE(  uint32_t uxTaskNumber, char* taskName )
{
	if(buffer_used > sizeof(buffer)-64)
		return; // buffer full, do not write

	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();
	buffer_used += sprintf( buffer+buffer_used, "CRE 0 %lu %lu\r\n", uxTaskNumber, GET_TICK() );
	buffer_used += sprintf( buffer+buffer_used, "NAM 0 %lu %s\r\n", uxTaskNumber, taskName );
	taskEXIT_CRITICAL_FROM_ISR(state);
}

void TimeDoctor_traceTASK_SWITCHED_IN( uint32_t uxTaskNumber )
{
	if(buffer_used > sizeof(buffer)-64)
		return; // buffer full, do not write

	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();
	buffer_used += sprintf( buffer+buffer_used, "STA 0 %lu %lu\r\n", uxTaskNumber, GET_TICK() );
	taskEXIT_CRITICAL_FROM_ISR(state);
}

void TimeDoctor_traceTASK_SWITCHED_OUT(uint32_t uxTaskNumber )
{
	if(buffer_used > sizeof(buffer)-64)
		return; // buffer full, do not write

	UBaseType_t state  = taskENTER_CRITICAL_FROM_ISR();
	buffer_used += sprintf( buffer+buffer_used, "STO 0 %lu %lu\r\n", uxTaskNumber, GET_TICK() );
	taskEXIT_CRITICAL_FROM_ISR(state);
}

void TimeDoctor_traceTASK_DELETE( uint32_t uxTaskNumber)
{
	if(buffer_used > sizeof(buffer)-64)
			return; // buffer full, do not write

	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();
	buffer_used += sprintf( buffer+buffer_used, "DEL 0 %lu %lu\r\n", uxTaskNumber, GET_TICK() );
	taskEXIT_CRITICAL_FROM_ISR(state);

}

void TimeDoctor_traceTASK_SUSPEND ( uint32_t uxTaskNumber){
	if(buffer_used > sizeof(buffer)-64)
			return; // buffer full, do not write

	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();
	buffer_used += sprintf( buffer+buffer_used, "STO 0 %lu %lu\r\n", uxTaskNumber, GET_TICK() );
	taskEXIT_CRITICAL_FROM_ISR(state);
}

void TimeDoctor_traceTASK_RESUME(  uint32_t uxTaskNumber)
{
	if(buffer_used > sizeof(buffer)-64)
			return; // buffer full, do not write

	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();
	buffer_used += sprintf( buffer+buffer_used, "STA 0 %lu %lu\r\n", uxTaskNumber, GET_TICK() );
	taskEXIT_CRITICAL_FROM_ISR(state);
}



/* TRACE-MACROS FOR QUEUE */

static unsigned long g_uxQueueNumber 		= 0x100; // global counter of created queues
static unsigned long g_uxSemaphoreNumber 	= 0x200; // global counter of created semaphores
static unsigned long g_uxMutexNumber 		= 0x300; // global counter of created mutexes

void TimeDoctor_traceQUEUE_CREATE( void *pxNewQueue )
{
	if(buffer_used > sizeof(buffer)-64)
			return; // buffer full, do not write

	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();

	QueueHandle_t* pxQueue = (QueueHandle_t*)pxNewQueue;

	vQueueSetQueueNumber( pxNewQueue, g_uxQueueNumber );
	buffer_used += sprintf( buffer+buffer_used, "CRE 3 %lu %lu\r\n", g_uxQueueNumber, GET_TICK() );
	g_uxQueueNumber++,

	taskEXIT_CRITICAL_FROM_ISR(state);
}

void TimeDoctor_traceQUEUE_REGISTRY_ADD( void *xQueue, uint32_t uxQueueNumber, const char* pcQueueName )
{
	if(buffer_used > sizeof(buffer)-64)
			return; // buffer full, do not write

	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();
	//buffer_used += sprintf( buffer+buffer_used, "NAM 3 %p %lu %s\r\n", xQueue, uxQueueNumber, pcQueueName);
	buffer_used += sprintf( buffer+buffer_used, "NAM 3 %lu %s\r\n", uxQueueNumber, pcQueueName);
	taskEXIT_CRITICAL_FROM_ISR(state);
}

void TimeDoctor_traceQUEUE_SEND(uint32_t uxQueueNumber, uint32_t uxMessagesWaiting)
{
	if(buffer_used > sizeof(buffer)-64)
			return; // buffer full, do not write

	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();
	buffer_used += sprintf( buffer+buffer_used, "STA 3 %lu %lu\r\n", uxQueueNumber, GET_TICK() );
	taskEXIT_CRITICAL_FROM_ISR(state);

}

void TimeDoctor_traceQUEUE_RECEIVE(uint32_t uxQueueNumber, uint32_t uxMessagesWaiting) /*The number of items currently in the queue. */
{
	if(buffer_used > sizeof(buffer)-64)
			return; // buffer full, do not write

	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();
	buffer_used += sprintf( buffer+buffer_used, "STO 3 %lu %lu\r\n", uxQueueNumber, GET_TICK() );
	taskEXIT_CRITICAL_FROM_ISR(state);
}

void TimeDoctor_traceQUEUE_DELETE( uint32_t uxQueueNumber )
{
	if(buffer_used > sizeof(buffer)-64)
			return; // buffer full, do not write

	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();
	buffer_used += sprintf( buffer+buffer_used, "DEL 3 %lu %lu\r\n", uxQueueNumber, GET_TICK() );
	taskEXIT_CRITICAL_FROM_ISR(state);
}

void TimeDoctor_traceQUEUE_SEND_FROM_ISR( uint32_t uxQueueNumber, uint32_t uxMessagesWaiting)
{
	if(buffer_used > sizeof(buffer)-64)
			return; // buffer full, do not write

	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();
	buffer_used += sprintf( buffer+buffer_used, "STA 3 %lu %lu\r\n", uxQueueNumber, GET_TICK() );
	taskEXIT_CRITICAL_FROM_ISR(state);
}

void TimeDoctor_traceQUEUE_RECEIVE_FROM_ISR( uint32_t  uxQueueNumber, uint32_t uxMessagesWaiting )
{
	if(buffer_used > sizeof(buffer)-64)
			return; // buffer full, do not write

	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();
	buffer_used += sprintf( buffer+buffer_used, "STO 3 %lu %lu\r\n", uxQueueNumber, GET_TICK() );
	taskEXIT_CRITICAL_FROM_ISR(state);
}


/* TRACE-MACROS FOR SEMAPHORE */
void TimeDoctor_traceCREATE_COUNTING_SEMAPHORE()
{
	if(buffer_used > sizeof(buffer)-64)
				return; // buffer full, do not write

	buffer_used += sprintf( buffer+buffer_used, "CRE 11 %lu\r\n", GET_TICK() );
}

/* TRACE-MACROS FOR MUTEX */

void TimeDoctor_traceCREATE_MUTEX( void *pxNewMutex )
{
	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();

	if(buffer_used > sizeof(buffer)-64)
				return; // buffer full, do not write

	QueueHandle_t* pxQueue = (QueueHandle_t*)pxNewMutex;

	vQueueSetQueueNumber( pxNewMutex, g_uxMutexNumber );
	buffer_used += sprintf( buffer+buffer_used, "CRE 3 %lu %lu\r\n", g_uxMutexNumber, GET_TICK() );
	g_uxMutexNumber++;

	taskEXIT_CRITICAL_FROM_ISR(state);

}

void TimeDoctor_traceTAKE_MUTEX_RECURSIVE(void *pxMutex, uint32_t uxQueueNumber )
{
	if(buffer_used > sizeof(buffer)-64)
			return; // buffer full, do not write

	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();
	buffer_used += sprintf( buffer+buffer_used, "STA 2 %lu %lu\r\n", uxQueueNumber, GET_TICK() );
	taskEXIT_CRITICAL_FROM_ISR(state);
}

void TimeDoctor_traceGIVE_MUTEX_RECURSIVE(void *pxMutex, uint32_t uxQueueNumber )
{
	if(buffer_used > sizeof(buffer)-64)
			return; // buffer full, do not write

	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();
	buffer_used += sprintf( buffer+buffer_used, "STO 2 %lu %lu\r\n", uxQueueNumber, GET_TICK() );
	taskEXIT_CRITICAL_FROM_ISR(state);
}


////////////////////// User Agent //////////////////////////

void TimeDoctor_traceUserAgentCreate ( uint32_t uxUserAgentNumber, char *name)
{
	if(buffer_used > sizeof(buffer)-64)
			return; // buffer full, do not write

	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();
	buffer_used += sprintf( buffer+buffer_used, "CRE 8 %lu %lu\r\n", uxUserAgentNumber, GET_TICK() );
	buffer_used += sprintf( buffer+buffer_used, "NAM 8 %lu %s\r\n", uxUserAgentNumber, name );
	taskEXIT_CRITICAL_FROM_ISR(state);
}

void TimeDoctor_traceUserAgentStart ( uint32_t uxUserAgentNumber )
{
	if(buffer_used > sizeof(buffer)-64)
			return; // buffer full, do not write

	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();
	buffer_used += sprintf( buffer+buffer_used, "STA 8 %lu %lu\r\n", uxUserAgentNumber, GET_TICK() );
	taskEXIT_CRITICAL_FROM_ISR(state);
}

void TimeDoctor_traceUserAgentString ( char *string )
{
	if(buffer_used > sizeof(buffer)-64)
			return; // buffer full, do not write

	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();
	// DSC <type> <id> <value>
	// string: type=0 we set id=0, could be different id's for more info
	buffer_used += sprintf( buffer+buffer_used, "DSC 0 0 %s\r\n", string );
	taskEXIT_CRITICAL_FROM_ISR(state);
}

void TimeDoctor_traceUserAgentNumber ( int number )
{
	if(buffer_used > sizeof(buffer)-64)
			return; // buffer full, do not write

	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();
	// DSC <type> <id> <value>
	// number: type=1 we set id=1, could be different id's for more info
	buffer_used += sprintf( buffer+buffer_used, "DSC 1 1 %d\r\n", number );
	taskEXIT_CRITICAL_FROM_ISR(state);
}

void TimeDoctor_traceUserAgentColor ( int color )
{
	if(buffer_used > sizeof(buffer)-64)
			return; // buffer full, do not write

	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();
	// DSC <type> <id> <value>
	// color: type=3 we set id=3, could be different id's for more info
	buffer_used += sprintf( buffer+buffer_used, "DSC 3 3 %d\r\n", color );
	taskEXIT_CRITICAL_FROM_ISR(state);
}

void TimeDoctor_traceUserAgentStop( uint32_t uxUserAgentNumber )
{
	if(buffer_used > sizeof(buffer)-64)
			return; // buffer full, do not write

	BaseType_t state = taskENTER_CRITICAL_FROM_ISR();
	buffer_used += sprintf( buffer+buffer_used, "STO 8 %lu %lu\r\n", uxUserAgentNumber, GET_TICK() );
	taskEXIT_CRITICAL_FROM_ISR(state);
}

